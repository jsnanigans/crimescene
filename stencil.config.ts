import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'crimescene',
  globalStyle: 'src/global/variables.css',
  copy: [
    { src: 'global' }
  ],
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader'
    },
    {
      type: 'docs-readme'
    },
    {
      type: 'www',
      serviceWorker: null // disable service workers
    }
  ],
};
