import {Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'cs-input',
  styleUrl: 'cs-input.css',
  shadow: true
})
export class CsInput {
  @Prop() value: string;
  @Prop({reflect: true}) error: string;

  onInput = () => {
    if (this.error !== null) {
      this.error = null
    }
  };

  renderError(): string {
    return this.error && <ul><li>{this.error}</li></ul>
  }

  render() {
    return <div><input value={this.value} onInput={this.onInput}/>
      {this.renderError()}
    </div>;
  }
}
