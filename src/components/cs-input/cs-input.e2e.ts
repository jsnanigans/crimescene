import { newE2EPage } from '@stencil/core/testing';

describe('my-component', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent('<cs-input></cs-input>');
    const element = await page.find('cs-input');
    expect(element).toHaveClass('hydrated');
  });

  it('renders changes to the name data', async () => {
    const page = await newE2EPage();

    await page.setContent('<cs-input></cs-input>');
    // const component = await page.find('cs-input');
    // const element = await page.find('cs-input >>> input');

    // component.setProperty('error', '');
    // await page.waitForChanges();
    // expect(element.textContent).toEqual(`Hello, World! I'm James`);
    //
    // component.setProperty('last', 'Quincy');
    // await page.waitForChanges();
    // expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);
    //
    // component.setProperty('middle', 'Earl');
    // await page.waitForChanges();
    // expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
  });
});
